var options = {
    timeout: 60000 // Time in milliseconds
}
exports.name = "relay1";
exports.inUse = false;
exports.timeOuts = [];
exports.environments = [];

exports.trigger = function(triggerName, data) {
    if(!exports.inUse) {

        // Check environments to see if this can run
        for (i in exports.environments) {
            if(!exports.environments[i].isOk()) {
                console.log("Can't run trigger due to "+exports.environments[i].name+" environment restriction");
                return;
            }
        }

        // Open relay for 5s
        openRelay();

        // Close relay after 5s
        exports.timeOuts.push(setTimeout(closeRelay, options.timeout));
    }
}

exports.cancel = function() {
    // Clear all set timers
    for(i in exports.timeOuts) {
        clearTimeout(exports.timeOuts[i])
    }
    closeRelay();
}

exports.setOption = function(option) {
    options[option.key] = option.value;
}

exports.addEnvironment = function(environment) {
    exports.environments.push(environment);
}

var openRelay = function() {
    exports.inUse = true;
}

var closeRelay = function() {
    exports.inUse = false;
}