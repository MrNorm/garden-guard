var options = {
    minTime: 5,
    maxTime: 22
}
exports.name = "time";

exports.isOk = function() {
    var date = new Date();
    var current_hour = date.getHours();

    return (current_hour > options.minTime && current_hour < options.maxTime);
}