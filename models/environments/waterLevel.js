var options = {
    minLevel: 20,
    maxLevel: 100
}
exports.name = "waterLevel";

exports.isOk = function() {

    var waterLevel = 50; // Grabbed from a sensor somewhere?

    return (waterLevel > options.minLevel && waterLevel < options.maxLevel);
}