var triggers = [];
var actions = [];
var environments = [];

exports.registerTriggers = function() {
    var className = null;

    console.log("Registering triggers");

    require("fs").readdirSync("./models/triggers").forEach(function(file) {
        className = file.replace(".js", "");
        triggers[className] = require("./triggers/" + file);

        if(triggers[className].setOption !== undefined) {
            triggers[className].setOption({
                key: "callback",
                value: exports.triggerActivated
            })
        }

        console.log("Registered "+className+" trigger");
    });

    return true;
}

exports.startTriggerMonitors = function() {
    var triggerName;
    for(i in triggers) {
        if(triggers[i] !== undefined && triggers[i].name !== undefined) {
            triggerName = triggers[i].name;
        }
        console.log("Starting "+triggerName+" monitor");
        if(triggers[i].startMonitor()) {
            console.log(triggerName+" monitor started");
        } else {
            console.log(triggerName+" monitor NOT started");
        }
    }
    return true;

}

exports.triggerActivated = function(triggerName, data) {
    if((typeof triggerName) === "string") {
        console.log("A trigger has been activated. Notify all actions");
        for(i in actions) {
            if(actions[i].trigger !== undefined && actions[i].name !== undefined) {
                console.log(actions[i].name+" action notified");
                actions[i].trigger(triggerName, data);
            }
        }
    }
}

exports.stopTriggers = function() {

    console.log("Stopping all triggers");
    for(i in actions) {
        if(actions[i].cancel !== undefined) {
            console.log(actions[i].name+" action cancelled");
            actions[i].cancel();
        }
    }

}

exports.setupTriggerMonitors = function(options) {
    if(options !== undefined && options.length > 0) {

        for(i in options) {
            var triggerName = options[i].trigger;
            if(triggerName !== undefined && triggers[triggerName].setOption !== undefined) {
                console.log("Setting "+options[i].key+" option for "+triggerName+" trigger");
                triggers[triggerName].setOption(options[i]);
            }
        }
    }

    return true;
}

exports.registerTriggerActions = function() {
    var className = null;

    console.log("Registering triggers");

    require("fs").readdirSync("./models/actions").forEach(function(file) {
        className = file.replace(".js", "");
        actions[className] = require("./actions/" + file);

        console.log("Registered "+className+" action");
    });

    return true;
}

exports.setupTriggerActions = function(options) {
    if(options !== undefined && options.length > 0) {

        for(i in options) {
            var actionName = options[i].action;
            if(actionName !== undefined && actions[actionName].setOption !== undefined) {
                console.log("Setting "+options[i].key+" option for "+actionName+" action");
                actions[actionName].setOption(options[i]);
            }
        }
    }

    return true;
}

exports.registerActionEnvironments = function() {
    var className = null;

    console.log("Registering triggers");

    require("fs").readdirSync("./models/environments").forEach(function(file) {
        className = file.replace(".js", "");
        environments[className] = require("./environments/" + file);

        console.log("Registered "+className+" environment");
    });

    return true;
}

exports.setupActionEnvironments = function(options) {
    if(options !== undefined && options.length > 0) {

        for(i in options) {
            var environmentName = options[i].environment;
            var actionName = options[i].action;

            if(
                environmentName !== undefined && environments[environmentName].isOk !== undefined && environments[environmentName].name !== undefined &&
                actionName !== undefined && actions[actionName].addEnvironment !== undefined
                ) {
                console.log("Adding "+environmentName+" environment to "+actionName+" action");
                actions[actionName].addEnvironment(environments[environmentName]);
            }
        }
    }

    return true;
}