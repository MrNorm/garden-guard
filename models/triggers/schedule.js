var i = 0;
var options = {
    enabled: false,
    checkInterval: 10000,
    schedule: [

    ]
};
exports.name = "schedule";
exports.description = "Runs actions at specified times"

exports.startMonitor = function() {
    setInterval(check, options.checkInterval);
    return true;
}

var check = function() {
    if(options.callback !== undefined) {

        //TODO specify a schedule name in callback
        var data = {};
        options.callback(exports.name, data);
    }
}

exports.setOption = function(option) {
    if(option.key === "schedules") {
        options[option.key] = JSON.parse(option.value);
    } else {
        options[option.key] = option.value;
    }
}