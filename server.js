var fs = require("fs");
var http = require("connect");
var monitor = require("./models/monitor.js");
var sqlite3 = require("sqlite3").verbose();
var db = null;

// TODO put this in a config somewhere
var file = "guard.db";
var webPort = 8080;
var wsPort = 8081;

var setup = function() {
    console.log("Check for an existing database");
    var exists = fs.existsSync(file);
    if(!exists) {
        createDatabase();
    } else {
        connectDatabase();
    }

    console.log("Starting web server");
    http().use(http.static(__dirname + '/html')).listen(webPort);

    console.log("Setting up monitor");
    if(monitor.registerTriggers() && monitor.registerTriggerActions() && monitor.registerActionEnvironments()) {

        db.serialize(function() {
            db.all("SELECT key, value, trigger FROM actionOptions", [], function(error, rows) {
                if(error !== null) {
                    console.log("An error has occurred getting action options: "+error);
                } else {
                    console.log("Setting action options from DB");
                    monitor.setupTriggerActions(rows);
                }
            });

            db.all("SELECT action, environment FROM actionEnvironments", [], function(error, rows) {
                if(error !== null) {
                    console.log("An error has occurred getting environment actions: "+error);
                }

                console.log("Setting action environments pairings from DB");
                if(monitor.setupActionEnvironments(rows)) {
                    console.log("Action environments paired");
                }
            });

            db.all("SELECT key, value, trigger FROM triggerOptions", [], function(error, rows) {
                if(error !== null) {
                    console.log("An error has occurred getting trigger options: "+error);
                }

                console.log("Setting trigger options from DB");
                if(monitor.setupTriggerMonitors(rows)) {
                    console.log("Starting monitor");
                    monitor.startTriggerMonitors();
                }
            });
        });
    }
}

var createDatabase = function() {
    console.log("Creating DB file");
    fs.openSync(file, "w");
    connectDatabase();
    db.serialize(function() {
        // Create config table and insert default values
        db.run("CREATE TABLE config (key TEXT, value TEXT)", function(error) {
            if(error !== null) {
               console.log("An error has occurred creating config table: "+error);
            }
        });
        db.run("INSERT INTO config VALUES ($key, $value)", {
            $key: 'version',
            $value: '0.0.1'
        }, function(error) {
            if(error !== null) {
                console.log("An error has occurred inserting the version number: "+error);
            }
        });

        // Create trigger options table
        db.run("CREATE TABLE triggerOptions (key TEXT, value TEXT, trigger TEXT)", function(error) {
            if(error !== null) {
                console.log("An error has occurred creating trigger options table: "+error);
            }
        });

        // Create actions options table
        db.run("CREATE TABLE actionOptions (key TEXT, value TEXT, action TEXT, trigger TEXT)", function(error) {
            if(error !== null) {
                console.log("An error has occurred creating action options table: "+error);
            }
        });

        // Create action environment table
        db.run("CREATE TABLE actionEnvironments (environment TEXT, action TEXT)", function(error) {
            if(error !== null) {
                console.log("An error has occurred creating action environment table: "+error);
            }
        });
    });
}

var connectDatabase = function() {
    console.log("Connecting database");
    db = new sqlite3.Database(file);
}

console.log("Running setup function");
setup();